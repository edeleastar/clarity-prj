import yaml
from Lab import Lab
from Topic import Topic
from utils.MarkdownUtils import parse_markdown
import os

class Course:
  def __init__(self, courseFile):
    f = open(courseFile)
    courseSpec = yaml.load(f)
    self.__dict__.update(**courseSpec)
    self.theLabs = []
    self.topics  = []
    if os.path.exists('index.md'):
      self.summary   = parse_markdown('index.md')
    else:
      self.summary = ''
    
    if len(self.labs) > 0:
      for labSpec in self.labs:
        lab = Lab(self, **labSpec)
        self.theLabs.append(lab)
      self.filterTopics()

  def filterTopics(self):
    if len(self.theLabs) == 1:
      topic = Topic(self, self.theLabs[0].htmlindex)
      topic.append(self.theLabs[0])
      self.topics.append(topic)
    else:
      i=0
      topic = None
      while i in range(len(self.theLabs)):
        if i <  len(self.theLabs):
          if self.theLabs[i].topicFolder != self.theLabs[i-1].topicFolder:
            topic = Topic(self, self.theLabs[i].htmlindex)
            topic.append(self.theLabs[i])
            self.topics.append(topic)
            i = i+1
          else:
            topic.append (self.theLabs[i])
            i = i+1
            if i < len(self.theLabs):
              while (self.theLabs[i].topicFolder == self.theLabs[i-1].topicFolder):
                topic.append (self.theLabs[i])
                i = i+1
