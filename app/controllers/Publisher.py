from LabPage     import LabPage
from TopicPage   import TopicPage
from CoursePage  import CoursePage

from utils.FileUtils  import copyStyle, copyFolder
from os import path
from shutil import rmtree
import jinja2
import settings
import shutil

class Publisher:
  
  def __init__(self, course, root):
    self.course = course
    self.root   = root 
    templateLoader = jinja2.FileSystemLoader( searchpath=settings.viewsPath )  
    self.env       = jinja2.Environment( loader=templateLoader, trim_blocks=True, lstrip_blocks=True, line_statement_prefix='#' )

  def genPublicPath(self, path):
    return self.root +  path[1:]
  
 
  def publishLab(self, lab):
    # Check if user has supplied a courseURL field. If yes, push it into lab object
    if hasattr(self.course, 'courseUrl'):
      lab.courseUrl = self.course.courseUrl
    else:
      # if not, set to parent topic page
      lab.courseUrl = "../../index.html"

 #   if settings.singlePageLabs:
      lab.labUrl = lab.labPath + '/html/index.html'
      page = LabPage(self.env, self.genPublicPath(lab.labPath + '/html/index.html'))  
      page.publishSinglePage(lab, self.course.contributors)
    # else:
    #   for step in lab.theSteps:
    #     page = LabPage(self.env, self.genPublicPath(step.stepHtml))  
    #     page.publish(lab, step, self.course.contributors)

    labPublicFolder = self.genPublicPath(lab.labPath)
    if (lab.theSteps > 0):
      copyStyle (labPublicFolder + '/html/style')      
      copyFolder(lab.imgFolder,       labPublicFolder + '/img')
      copyFolder(lab.archivesFolder,  labPublicFolder + '/archives')

    shutil.make_archive(self.genPublicPath(lab.topicPath)+'/' + lab.labFolder + '-archive', format="zip", root_dir=labPublicFolder)     
 
  def publishMoodleLab(self, lab):
    # Check if user has supplied a courseURL field. If yes, push it into lab object
    if hasattr(self.course, 'courseUrl'):
      lab.courseUrl = self.course.courseUrl
    else:
      # if not, set to parent topic page
      lab.courseUrl = "../../index.html"
    for step in lab.theSteps:
      page = LabPage(self.env, self.genPublicPath(step.bookStepHtml))  
      page.publishBook(lab, step, self.course.contributors)

    labPublicFolder = self.genPublicPath(lab.bookPath)
    if (lab.theSteps > 0):
    #  copyStyle (labPublicFolder + '/style')      
      copyFolder(lab.imgFolder,       labPublicFolder + '/img')
      copyFolder(lab.archivesFolder,  labPublicFolder + '/archives')   

    bookArchive = self.genPublicPath(lab.topicPath)+'/' + lab.labFolder + '-book'
    shutil.make_archive(bookArchive, format="zip", root_dir=labPublicFolder)    
    rmtree (labPublicFolder)


  def publishTopic(self, topic):
    for lab in topic.labs:
      self.publishLab(lab)
    if settings.moodle:
      for lab in topic.labs:
        self.publishMoodleLab(lab)

    print (topic.title)
    
    topicPublicFolder = self.genPublicPath(topic.topicPath)  
    topicPage = TopicPage(self.env, topicPublicFolder + '/index.html')   
    topicPage.publish(self.course, topic)  
    copyStyle (topicPublicFolder + '/style')  
    copyFolder(topic.pdfFolder, topicPublicFolder + '/pdf')
    
  def publishTopicNmr (self, topicNumber):
    topic = self.course.topics[topicNumber];
    self.publishTopic(topic)
            
  def publishCourse(self):
    for topic in self.course.topics:
      self.publishTopic(topic)
    coursePage = CoursePage(self.env, self.root + '/index.html')
    coursePage.publish(self.course, self.course.contributors)  
    copyStyle (self.root + '/style')  
    
  def clean(self):
    if (path.exists(self.root)):
      rmtree (self.root, True)    
      