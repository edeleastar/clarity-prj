from utils.FileUtils import writePage
import settings

class LabPage:
  def __init__(self, env, path):
    self.path  = path
    self.env   = env  
 
  def publish (self, lab, step, contributors):
    template = self.env.get_template('step.html')
    writePage(self.path, template.render(title=lab.title, lab=lab, thisStep=step, contributors=contributors, courseUrl=lab.courseUrl, tracking=settings.tracking, moodle=settings.moodle))

  def publishBook (self, lab, step, contributors):
    template = self.env.get_template('bookstep.html')
    if hasattr (lab.course, 'gaTracking'):
      writePage(self.path, template.render(title=lab.title, lab=lab, thisStep=step, contributors=contributors, courseUrl=lab.courseUrl, tracking=settings.tracking, gaTracking=lab.course.gaTracking, gaDomain=lab.course.gaDomain))
    else:
      writePage(self.path, template.render(title=lab.title, lab=lab, thisStep=step, contributors=contributors, courseUrl=lab.courseUrl))

  def publishSinglePage (self, lab, contributors):
    template = self.env.get_template('singlepagelab.html')
    if hasattr (lab.course, 'gaTracking'):
      writePage(self.path, template.render(title=lab.title, lab=lab, contributors=contributors, courseUrl=lab.courseUrl, tracking=settings.tracking, gaTracking=lab.course.gaTracking, gaDomain=lab.course.gaDomain))
    else:
      writePage(self.path, template.render(title=lab.title, lab=lab, contributors=contributors, courseUrl=lab.courseUrl))
