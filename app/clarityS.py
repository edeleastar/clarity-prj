#!/usr/bin/env python
if __name__ == "__main__":
  from models.Course import Course   
  from controllers.Publisher import Publisher
  from argparse import ArgumentParser
  import os
  import settings

  print ('Clarity Version ' + settings.version + ' (-h for commands)')

  parser = ArgumentParser()
  parser.add_argument("-clean",          help="remove generated files",                            action="store_true")
  parser.add_argument("-verbose",        help="show details of generated files",                   action="store_true")
  parser.add_argument("-moodle",         help="generates format suitable for moodle book/chapter", action="store_true")
  parser.add_argument("-tracking",       help="add google tracking for analytics",                 action="store_true")
  parser.add_argument("lab",             help="build a specific lab (by number)",                  nargs='?', type=int)

  args = parser.parse_args()
  
  settings.verbose        = args.verbose 
  settings.moodle         = args.moodle
  settings.tracking       = args.tracking
  settings.viewsPath      = os.path.dirname(os.path.realpath(__file__)) + '/views'

  if (os.path.isfile('index.yaml') == False):
    print ('Cannot locate index.yaml')
  else:
    course    = Course('index.yaml')
    if hasattr(course, 'publishFolder'):
      folder = course.publishFolder
    else:
      folder = './public'
    publisher = Publisher(course, folder)
    settings.contributors = course.contributors
    if args.clean:
      print ('removing generated files...')
      publisher.clean()
    else:
      if args.lab:
        topicNumber = args.lab - 1
        if topicNumber >= 0 and topicNumber < len(course.topics):
          publisher.publishTopicNmr(topicNumber)
        else:
          print ('lab number out of range')   
      else:  
        publisher.publishCourse()
    print('done.')
