##Introduction

This is the home page for the clarityS course creation tool. It is designed to generate a static web site from a set of course materials, largely written in Markdown format.

A demo of the system is visible here:

- [clarity-demo](http://edel020.bitbucket.org/clarity-demo/index.html)

This demo course can be cloned from this public repo:

- [clarity-demo-repo](https://bitbucket.org/edel020/clarity-demo)

or directly downloaded as a zip here:

- [zip](https://bitbucket.org/edel020/clarity-demo/get/aa8bfb78ae41.zip)

##Installation

The system requires Python 2.7 to be installed on your system. In addition, the python installation will need the 'easy_install' utility to be included on the path. If these are in place, then the following commands will include in python the requisite libraries:

~~~
easy_install markdown
easy_install pyyaml
easy_install jinja2
~~~

The system will not run unless these are installed successfully. 

Clone this repository:

 - [clarity-prj](https://bitbucket.org/edel020/clarity-prj)

 or just download and unarchive a zipped version:

 - [clarity-prj.zip](https://bitbucket.org/edel020/clarity-prj/get/577fb443e35c.zip)

If running on mac/linux, place './clarity-prj/app' on the path. If running windows, make sure the python root directory is on the path.

##Test

To test the system, clone the demo repository:

- [clarity-demo-repo](https://bitbucket.org/edel020/clarity-demo) ([zip](- [zip](https://bitbucket.org/edel020/clarity-demo/get/aa8bfb78ae41.zip)))

Run a shell and change into the clarity-demo folder.

On mac/linux, just enter:

~~~
clarityS.py
~~~

On windows, you will need to provide an explicit path to the clarity script:

~~~
python ../clarity-prj/app/clarityS.py
~~~

If all goes smoothly, you should see on the console a list of the topics as they are generated:

~~~
Clarity Version 0.3 (-h for commands)
Unit Testing
UML Modeling & JPA
Git Repositories
Databases & JPA
HTTP APIs
API Test + Android App Basics
Android App Structure
done.
~~~

Open the ./public/index.html folder, and open index.html. It should resemble:

- [clarity-demo](http://edel020.bitbucket.org/clarity-demo/index.html)

##Commands

There is a help command:

~~~
$ clarityS.py -h
Clarity Version 0.3 (-h for commands)
usage: clarityS.py [-h] [-clean] [-verbose] [lab]

positional arguments:
  lab         build a specific lab (by number)

optional arguments:
  -h, --help  show this help message and exit
  -clean      remove generated files
  -verbose    show details of generated files
~~~

-c will remove the generated folder.
-v will trickle to the console the files being generated
.. and a specific lab cab be built by specifying its (1 based) number

##YAML Course Format

The yaml file is upward compatible from earlier versions. Here is a typical structure:

~~~
title         : Mobile Application Development
programme     : Higher Diploma in Computer Science (2013-14)
contributors  : Eamonn de Leastar (edeleastar@wit.ie)  
publishFolder : /Users/edeleastar/repos/modules/edel020.bitbucket.org/clarity-demo

labs          :

- title       : 'Unit Testing'    
  topicFolder : session01
  labFolder   : lab
  steps:
          - markdown : objectives
            title    : Objectives
          - markdown : step1
            title    : '1'
          - markdown : step2                                
            title    : '2'
          - markdown : step3
            title    : '3'
          - markdown : step4
            title    : '4'
          - markdown : step5
            title    : '5'
          - markdown : exercises
            title    : 'Exercises'    
~~~

Note that there are two new fields in the opening section:

- programme: if present this will be displayed in the banner
- publishFolder: if present, this is where the generted course will be written to.

Clarity will look in each folder for the following:

- index.md: if found, this is assumed to be a topic summary
- pdf: if this is a folder containing pdfs, then these will be listed in the topic summary page

Additionally, if a single index.md is found in the root of the module then this will be rendered in the course start page.

Also, the system permits more than one lab per 'topic', as in this specification here:

~~~
- title       : 'API Test + Android App Basics'    
  topicFolder : session06
  labFolder   : lab
  steps:
          - markdown : objectives
            title    : Objectives    
          - markdown : step1
            title    : '1'
          - markdown : step2                                
            title    : '2'
          - markdown : step3
            title    : '3'
          - markdown : step4
            title    : '4'
          - markdown : step5
            title    : '5'
          - markdown : step6
            title    : '6'
          - markdown : step7
            title    : '7'    
          - markdown : step8
            title    : '8'                               
          - markdown : xercises
            title    : 'Exercises' 

- title       : 'API Test + Android App Basics'    
  topicFolder : session06
  labFolder   : lab-android
  steps:
          - markdown : objectives
            title    : Objectives    
          - markdown : step1
            title    : '1'
          - markdown : step2                                
            title    : '2'
          - markdown : step3
            title    : '3'
          - markdown : step4
            title    : '4'
          - markdown : step5
            title    : '5'
          - markdown : step6
            title    : '6'
          - markdown : step7
            title    : '7'                       
          - markdown : xercises
            title    : 'Exercises'
~~~

Note that both of the above labs are in 'session06' topicFolder (this is in the demo course). The published effect is here:

- [2 lab topic example](http://edel020.bitbucket.org/clarity-demo/session06/index.html)

There is no formal 'topic' as such in the yaml file, topics are inferred as above.


##Customisation

The 'clarity-prj/app/view' folder 

- [views](https://bitbucket.org/edel020/clarity-prj/src/d1ea2ece4eb2985e2f1241587f63fe9aa0f8248f/app/views/?at=master)

contains templates adhering to the [jinja2](http://jinja.pocoo.org/docs/templates/) templating language. These four templates are defined:

- step.html
- topic.html
- course.html
- main.html

You can modify these to change style, introduce new elements etc. Please note, though, that if you edit these they may be overwritten if you pull on clairity-prj repo again, so back them up before doing so.

Additionally, there are other opportunities for customisation in this folder '/clarity-prj/public/custom/', where you can augment the existing main.css and main.js files. Again these will be overwritten by a pull on the repo, so dont loose your changes in that event.

##Hosting

###On Moodle:

The generated lab folders can be archived, dropped straight onto a moodle topic, and unarchived there. Set the 'main page' to the first html page of the lab. It is probably not feasible at the moment to upload a complete course onto moodle as such. Set the default for the link to 'open' instead of automatic, and the lab page will have a full browser tab.

###New for 0.4: Moodle Book support

If you use the '-m' switch, then a single archive per lab called 'book.zip' is placed in the public folder for each lab. This can be directly imported into a moodle book resource. To engage this in moodle, create a 'book', and, once created, select 'import chapter'. You can then drag/drop the book archive into the import panel in moodle. Press 'import' and then 'continue' once complete (takes a minute or so). 

The lab will not be fully encapsulated into moodle - retaining the styling faithfuly. However, it will now have a TOC long the left (the menu bar is removed), an ability to 'print books', analytics etc... This feature is in parallel to existing features, which remain unaltered.

The order of the 'chapters' (steps in the labs) is alphabetical, so the file names may have to be artificially named to ensure correct order. (eg xercises.md instead of exercises.md). The titles are used in the TOC, so those names will not be revealed to users.

#####New for 0.5: Google Analytics

If you pass a -t switch then standard google tracking code is generated for the site. The switch requires two properties to be set in the yaml file:

~~~
gaTracking    : UA-1234567-8 
gaDomain      : example.com
~~~

These can be acquired in the google analytics dashboard.

#####New for 0.6

The static web labs are now single page - with tabs switching between views. This does not effect the existing styles.

###On Bitbucket:

Bitbucket have a simple mechanism for hosting static sites:

- [Hosting Static Sites on Bitbucket](https://confluence.atlassian.com/display/BITBUCKET/Publishing+a+Website+on+Bitbucket)

Essentially,  you just commit the _generated_ course to a repo named 'yourbitbucketaccountname.bitbucket.org' - and thats it. It will be available on 'yourbitbucketaccountname.bitbucket.org'. It would be best to put the course into its own folder inside that repo, and you can publish other courses alongside it to the same repo (this is discussed in the link above).

When you regenerate a full course using clarityS, git is smart enough to only consider the differences as candidates to be committed. So updating a site is fairly quick.

##Future plans

The course format - yaml - file is becoming a bit unwieldy, and a future version will explore the idea of moving each lab specification into its own yaml file.

Have fun! All bug reports, comments and suggestions welcome
